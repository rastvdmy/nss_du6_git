# Java Learn

By Dmitry Rastvorov

A Simple Java programm for learn.

### Technologies Used
- Java

### Description
This programm will help You to learn and understand logic of Java programming.

Just going step by step You will become from beginner to professional in Java development just in 3 months!

By completing each lesson You will also receive virtual prizes that will motivate to complete the following tasks.

### Setup/Instalation Requirements 
- Clone this repository to Your desktop.
- Open folder **JavaLearn/src/main/launch**.
- Open a terminal in the folder that contains this program by right-clicking and selecting "open terminal in this folder".
- Write in terminal **javac Launch.java**.
- After that You will see the application on your desktop, which you can launch simply by clicking on the shortcut itself.
- Then just delete the repository and you're ready to go.

### Copyright
&copy; Dmitry Rastvorov, CVUT FEL SIT 2022
